import 'package:contacttracer/bloc/OwnerBloc/bloc.dart';
import 'package:contacttracer/bloc/PersonListBloc/bloc.dart';
import 'package:contacttracer/bloc/LocationBloc/bloc.dart';
import 'package:contacttracer/db/DBProvider.dart';
import 'package:contacttracer/ui/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/EncounterBloc/bloc.dart';
import 'ui/HomePage.dart';



class SimpleBlocObserver extends BlocObserver {
  @override
  void onEvent(Bloc bloc, Object event) {
    print(event);
    super.onEvent(bloc, event);
  }

  @override
  void onTransition(Cubit bloc, Transition transition) {
    print(transition);
    super.onTransition(bloc, transition);
  }

  @override
  void onError(Cubit bloc, Object error, StackTrace stackTrace) {
    print(error);
    super.onError(bloc, error, stackTrace);
  }
}


void main() {

  final DBProvider dbProvider = DBProvider();
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp( MultiBlocProvider(
        providers: [
          BlocProvider<PersonListBloc>(
            create: (context) {
              return PersonListBloc(
                dbprovider: dbProvider,
              )..add(PersonListEventRefresh());
            },
          ),
          BlocProvider<OwnerBloc>(
            create: (context) {
              return OwnerBloc(
                dbprovider: dbProvider,
              )..add(OwnerEventRefresh());
            },
          ),
          BlocProvider<LocationBloc>(
            create: (context) {
              return LocationBloc(
                dbprovider: dbProvider,
              )..add(LocationEventRefresh());
            },
          ),
          BlocProvider<EncounterBloc>(
            create: (context) {
              return EncounterBloc(
                dbprovider: dbProvider,
              )..add(EncounterEventRefresh());
            },
          ),
        ],
        child: ContactTracerApp(),
    ));
  });


}

class ContactTracerApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: mypalette,
        // primary color: 00F1FF
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}
