import 'package:flutter/material.dart';

const MaterialColor mypalette = MaterialColor(_mypalettePrimaryValue, <int, Color>{
  50: Color(0xFFE0FDFF),
  100: Color(0xFFB3FBFF),
  200: Color(0xFF80F8FF),
  300: Color(0xFF4DF5FF),
  400: Color(0xFF26F3FF),
  500: Color(_mypalettePrimaryValue),
  600: Color(0xFF00EFFF),
  700: Color(0xFF00EDFF),
  800: Color(0xFF00EBFF),
  900: Color(0xFF00E7FF),
});
const int _mypalettePrimaryValue = 0xFF00F1FF;

const MaterialColor mypaletteAccent = MaterialColor(_mypaletteAccentValue, <int, Color>{
  100: Color(0xFFFFFFFF),
  200: Color(_mypaletteAccentValue),
  400: Color(0xFFBFF8FF),
  700: Color(0xFFA6F5FF),
});
const int _mypaletteAccentValue = 0xFFF2FEFF;