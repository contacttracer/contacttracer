import 'package:contacttracer/bloc/LocationBloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'LocationCard.dart';
import 'LocationManagePage.dart';

class LocationListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<LocationBloc, LocationState>(
        builder: (context, state){
          Widget returnWidget = Container();
          if (state is LocationStateInitial) {
            returnWidget = Center(
              child: Text("Initial"),
            ); //HomePage();
          }
          if (state is LocationStateLoading) {
            returnWidget = Center(
              child: CircularProgressIndicator(),
            ); //HomePage();
          }
          if (state is LocationStateFailure){
            returnWidget = Center(
              child: Icon(CupertinoIcons.exclamationmark_triangle_fill),);
          }
          if (state is LocationStateDone){
            returnWidget = ListView.builder(
                itemCount: state.locations.length,
                itemBuilder: (context, index){
                  return LocationCard(location: state.locations[index]);
                }
            );
          }

          return returnWidget;
        },
      ),
        floatingActionButton: FloatingActionButton(
          child: Icon(CupertinoIcons.add),
          onPressed: (){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => LocationManagePage(),
              ),
            );
          },
        )
    );
  }
}
