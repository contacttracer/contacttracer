import 'package:contacttracer/models/Location.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'LocationManagePage.dart';


class LocationCard extends StatelessWidget{
  /*
  Renders a Card to represent a person in a list. If the person is the owner of the app, it
  should be styled differently.
  */
  final Location location;
  LocationCard({@required this.location }): assert(location != null);

  @override
  Widget build(BuildContext context) {
    var name = "???";
    var description = "";
    var id = -1;
    if (location.name != null){
      name = location.name;
    }
    if (location.description != null){
      description = location.description;
    }
    if (location.id != null){
      id = location.id;
    }

    return Card(
      child: ListTile(
        leading: Icon(CupertinoIcons.location),
        title: Text(name),
        subtitle: Text(description),
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => LocationManagePage(location: location,),
            ),
          );
        },
      ),
    );
  }


}