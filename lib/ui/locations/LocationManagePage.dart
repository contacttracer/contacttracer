import 'package:contacttracer/bloc/LocationBloc/bloc.dart';
import 'package:contacttracer/models/Location.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class LocationManagePage extends StatefulWidget{
  Location location;

  LocationManagePage({this.location});

  @override
  State<StatefulWidget> createState() {

    if (location != null){
      return LocationManagePageState(location: location);
    } else{
      return LocationManagePageState(location: new Location());
    }
  }


}

class LocationManagePageState extends State<LocationManagePage>{

  Location location;


  LocationManagePageState({@required this.location});

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: location.id == null ? Text("Add new Location") : Text("Update Location"),
      ),
      body: Column(
        children: [
          Column(
            children: [
              Container(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Name',
                  ),
                  enabled: true,
                  controller: new TextEditingController.fromValue(
                    new TextEditingValue(
                      text: location.name != null ? location.name : "",
                      selection: new TextSelection.collapsed(
                          offset: location.name != null ? location.name.length : 0),
                    ),
                  ),
                  onChanged: (text) {
                      print(text);
                      location.name= text;
                      if (location.id != null){
                        BlocProvider.of<LocationBloc>(context)..add(LocationEventSave(location: location));
                      }
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Description',
                  ),
                  enabled: true,
                  controller: new TextEditingController.fromValue(
                    new TextEditingValue(
                      text: location.description != null ? location.description : "",
                      selection: new TextSelection.collapsed(
                          offset: location.description != null ? location.description.length : 0),
                    ),
                  ),
                  onChanged: (text) {
                      print(text);
                      location.description = text;
                      if (location.id != null){
                        BlocProvider.of<LocationBloc>(context)..add(LocationEventSave(location: location));
                      }
                  },
                ),
              ),
            ],
          ),
          ButtonBar(
            buttonPadding: EdgeInsets.all(5.0),
            alignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                child: Text("Done"),
                onPressed: () {
                  BlocProvider.of<LocationBloc>(context)..add(LocationEventSave(location: location));
                  Navigator.of(context).pop();
                },
              ),
            ],
          )
        ],
      ),
      floatingActionButton: location.id != null ? SpeedDial(
        marginRight: 18,
        marginBottom: 20,
        animatedIcon: AnimatedIcons.menu_close,
        // this is ignored if animatedIcon is non null
        // child: Icon(Icons.add),
        visible: true,
        // If true user is forced to close dial manually
        // by tapping main button and overlay is not rendered.
        closeManually: false,
        overlayOpacity: 0.5,
        heroTag: 'speed-dial-hero-tag',
        elevation: 8.0,
        shape: CircleBorder(),
        children: [
          SpeedDialChild(
            child: Icon(CupertinoIcons.delete),
            backgroundColor: Colors.redAccent,
            onTap: () {
              BlocProvider.of<LocationBloc>(context).add(LocationEventDelete(location: location));
              Navigator.of(context).pop();
            },
          ),
        ],
      ) : null,
    );
  }

}