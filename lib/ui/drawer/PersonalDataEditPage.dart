import 'package:contacttracer/bloc/OwnerBloc/OwnerEvent.dart';
import 'package:contacttracer/bloc/OwnerBloc/bloc.dart';
import 'package:contacttracer/models/Person.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PersonalDataEditPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Personal Data"),
      ),
      body: Column(
        children: [
          Card(
            child: Container(
              padding: EdgeInsets.all(10),
              child: Text(
                  "This data is used to generate the qr code displayed by this app. "
                      "It is potentially shared with a lot of people. Feel free to "
                      "leave some fields empty if you like, but it is advisable to "
                      "tell the truth in at least one field, so that you can be "
                      "contacted should someone you have met be diagnosed."
              ),
            ),
          ),
          BlocBuilder<OwnerBloc, OwnerState>(
              builder: (context, state){
                Widget returnWidget = Container();

                if (state is OwnerStateInitial){
                  returnWidget = Center(child: Text("Initial State"),);
                }
                if (state is OwnerStateLoading){
                  returnWidget = Center(child: CircularProgressIndicator(),);
                }
                if (state is OwnerStateFailure){
                  returnWidget = Center(child: Icon(CupertinoIcons.exclamationmark_triangle_fill),);
                }
                if (state is OwnerStateDone){
                  Person owner = state.owner;
                  returnWidget = Column(
                    children: [
                      Container(
                        padding: EdgeInsets.all(5.0),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'First Name',
                          ),
                          enabled: true,
                          controller: TextEditingController(
                            text: owner.first_name!= null ? owner.first_name : "",
                          ),
                          onChanged: (text) {
                            owner.first_name = text;
                          },
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(5.0),
                        child: TextFormField(
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Last Name',
                          ),
                          enabled: true,
                          controller: TextEditingController(
                            text: owner.last_name != null ? owner.last_name : "",
                          ),
                          onChanged: (text) {
                            owner.last_name = text;
                          },
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(5.0),
                        child: TextFormField(
                          keyboardType: TextInputType.phone,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Phone',
                          ),
                          enabled: true,
                          controller: TextEditingController(
                            text: owner.phone != null ? owner.phone : "",
                          ),
                          onChanged: (text) {
                            owner.phone = text;
                          },
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(5.0),
                        child: TextFormField(
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'E-Mail',
                          ),
                          enabled: true,
                          controller: TextEditingController(
                            text: owner.email != null ? owner.email : "",
                          ),
                          onChanged: (text) {
                            owner.email = text;
                          },
                        ),
                      ),
                      ButtonBar(
                        buttonPadding: EdgeInsets.all(5.0),
                        alignment: MainAxisAlignment.center,
                        children: [
                          ElevatedButton(
                            child: Text("Save"),
                            onPressed: () {
                              BlocProvider.of<OwnerBloc>(context)..add(OwnerEventSave(owner: owner));
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      )
                    ],
                  );
                }
                return returnWidget;
              }
          ),
        ],
      ),
    );
  }
}




