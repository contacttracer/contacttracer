import 'package:contacttracer/bloc/EncounterBloc/bloc.dart';
import 'package:contacttracer/bloc/LocationBloc/bloc.dart';
import 'package:contacttracer/bloc/OwnerBloc/bloc.dart';
import 'package:contacttracer/bloc/PersonListBloc/bloc.dart';
import 'package:contacttracer/db/DBProvider.dart';
import 'package:clipboard/clipboard.dart';
import 'package:csv/csv.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:share/share.dart';

import 'PersonalDataEditPage.dart';

class PrimaryDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    DBProvider _dbprovider = new DBProvider();

    void _showDeleteAllDialog() {
      showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text("Delete all data"),
            content: Text(
                "If you press delete, all data collected using this app is deleted. This cannot be undone."),
            actions: [
              CupertinoButton(
                child: Text(
                  "Delete",
                  style: TextStyle(color: Colors.redAccent),
                ),
                onPressed: () async {
                  await _dbprovider.deleteDB();
                  BlocProvider.of<OwnerBloc>(context)..add(OwnerEventRefresh());
                  BlocProvider.of<PersonListBloc>(context)
                    ..add(PersonListEventRefresh());
                  BlocProvider.of<LocationBloc>(context)
                    ..add(LocationEventRefresh());
                  BlocProvider.of<EncounterBloc>(context)
                    ..add(EncounterEventRefresh());
                  Navigator.of(context).pop();
                },
              ),
              CupertinoButton(
                child: Text("Abort"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    void _showCSVExportDialog() {
      showCupertinoDialog(
        context: context,
        builder: (BuildContext context) {
          return CupertinoAlertDialog(
            title: Text("CSV Export"),
            content: FutureBuilder(
              future: _dbprovider.exportToCSV(),
              builder: (context, snapshot) {
                Widget returnWidget = Center(
                  child: CircularProgressIndicator(),
                );

                if (snapshot.hasData) {
                  final res = const ListToCsvConverter().convert(snapshot.data);
                  returnWidget = Column(
                    children: [
                      FlatButton(
                        child: Text("Share"),
                        onPressed: () {
                          Share.share(res, subject: 'My personal contacts');
                        },
                      ),
                      FlatButton(
                        child: Text("Copy to Clipboard"),
                        onPressed: () {
                          FlutterClipboard.copy(res)
                              .then((value) => print('copied'));
                        },
                      ),
                      Text("$res"),
                    ],
                  );
                }

                if (snapshot.hasError) {
                  returnWidget = Text(snapshot.error.toString());
                }

                return returnWidget;
              },
            ),
            actions: [
              CupertinoButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }

    return Drawer(
      child: ListView(
        children: <Widget>[
          DrawerHeader(
            child: Text("Contact Tracer Menu"),
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
          ),
          FlatButton(
            child: Text("Personal Data"),
            onPressed: () {
              Navigator.of(context).pop();
              // show personal data page
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PersonalDataEditPage(),
                ),
              );
            },
          ),
          FlatButton(
            child: Text("About"),
            onPressed: () {
              Navigator.of(context).pop();
              showAboutDialog(
                  applicationName: "Contact Tracer",
                  applicationVersion: '1.0.0',
                  applicationIcon: Image.asset(
                    "assets/icon_ios.png",
                    width: 70,
                    height: 70,
                  ),
                  context: context);
            },
          ),
          Divider(),
          FlatButton(
            child: Text("Delete all data"),
            color: Colors.redAccent,
            onPressed: () {
              Navigator.of(context).pop();
              _showDeleteAllDialog();
            },
          ),
          FlatButton(
            child: Text("CSV EXPORT"),
            onPressed: () {
              Navigator.of(context).pop();
              _showCSVExportDialog();
            },
          ),
        ],
      ),
    );
  }
}
