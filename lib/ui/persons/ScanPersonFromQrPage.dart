import 'package:contacttracer/bloc/PersonListBloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

const flashOn = 'FLASH ON';
const flashOff = 'FLASH OFF';
const frontCamera = 'FRONT CAMERA';
const backCamera = 'BACK CAMERA';

class ScanPersonFromQrPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ScanPersonFromQrPageState();
  }
}

class ScanPersonFromQrPageState extends State<ScanPersonFromQrPage> {
  List<String> scannedData = [];

  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController controller;
  var cameraState = frontCamera;

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      if (scannedData.contains(scanData)) {
        // do nothing
      } else {
        setState(() {
          scannedData.add(scanData);
          BlocProvider.of<PersonListBloc>(context)
              .add(PersonListEventSavePersonFromQr(data: scanData));
        });
      }
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create person from QR code"),
      ),
      body: ListView(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.width,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
              overlay: QrScannerOverlayShape(
                borderColor: Colors.red,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: 300,
              ),
            ),
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Text("Scanned Persons: ${scannedData.length}", style: TextStyle(fontSize: 20),),
            ),
          )
        ],
      ),
    );
  }
}
