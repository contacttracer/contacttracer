import 'package:contacttracer/bloc/PersonListBloc/bloc.dart';
import 'package:contacttracer/models/Person.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class PersonManagePage extends StatefulWidget{
  Person person;

  PersonManagePage({this.person});

  @override
  State<StatefulWidget> createState() {

    if (person != null){
      return PersonManagePageState(person: person);
    } else{
      return PersonManagePageState(person: new Person());
    }
  }


}

class PersonManagePageState extends State<PersonManagePage>{

  Person person;


  PersonManagePageState({@required this.person});

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: person.id == null ? Text("Add new Person") : Text("Update Person"),
      ),
      body: Column(
        children: [
          Column(
            children: [
              Container(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'First Name',
                  ),
                  enabled: true,
                  controller: new TextEditingController.fromValue(
                    new TextEditingValue(
                      text: person.first_name != null ? person.first_name : "",
                      selection: new TextSelection.collapsed(
                          offset: person.first_name != null ? person.first_name.length : 0),
                    ),
                  ),
                  onChanged: (text) {
                      print(text);
                      person.first_name = text;
                      if (person.id != null){
                        BlocProvider.of<PersonListBloc>(context)..add(PersonListEventSavePerson(person: person));
                      }
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Last Name',
                  ),
                  enabled: true,
                  controller: new TextEditingController.fromValue(
                    new TextEditingValue(
                      text: person.last_name != null ? person.last_name : "",
                      selection: new TextSelection.collapsed(
                          offset: person.last_name != null ? person.last_name.length : 0),
                    ),
                  ),
                  onChanged: (text) {
                      print(text);
                      person.last_name = text;
                      if (person.id != null){
                        BlocProvider.of<PersonListBloc>(context)..add(PersonListEventSavePerson(person: person));
                      }
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Phone',
                  ),
                  enabled: true,
                  controller: new TextEditingController.fromValue(
                    new TextEditingValue(
                      text: person.phone != null ? person.phone : "",
                      selection: new TextSelection.collapsed(
                          offset: person.phone != null ? person.phone.length : 0),
                    ),
                  ),
                  onChanged: (text) {
                      print(text);
                      person.phone = text;
                      if (person.id != null){
                        BlocProvider.of<PersonListBloc>(context)..add(PersonListEventSavePerson(person: person));
                      }                  },
                ),
              ),
              Container(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'E-Mail',
                  ),
                  enabled: true,
                  controller: new TextEditingController.fromValue(
                    new TextEditingValue(
                      text: person.email != null ? person.email : "",
                      selection: new TextSelection.collapsed(
                          offset: person.email != null ? person.email.length : 0),
                    ),
                  ),
                  onChanged: (text) {
                      print(text);
                      person.email = text;
                      if (person.id != null){
                        BlocProvider.of<PersonListBloc>(context)..add(PersonListEventSavePerson(person: person));
                      }                  },
                ),
              ),
            ],
          ),
          ButtonBar(
            buttonPadding: EdgeInsets.all(5.0),
            alignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                child: Text("Done"),
                onPressed: () {
                  BlocProvider.of<PersonListBloc>(context)..add(PersonListEventSavePerson(person: person));
                  Navigator.of(context).pop();
                },
              ),
            ],
          )
        ],
      ),
      floatingActionButton: person.id != null ? SpeedDial(
        marginRight: 18,
        marginBottom: 20,
        animatedIcon: AnimatedIcons.menu_close,
        // this is ignored if animatedIcon is non null
        // child: Icon(Icons.add),
        visible: true,
        // If true user is forced to close dial manually
        // by tapping main button and overlay is not rendered.
        closeManually: false,
        overlayOpacity: 0.5,
        heroTag: 'speed-dial-hero-tag',
        elevation: 8.0,
        shape: CircleBorder(),
        children: [
          SpeedDialChild(
            child: Icon(CupertinoIcons.delete),
            backgroundColor: Colors.redAccent,
            onTap: () {
              BlocProvider.of<PersonListBloc>(context).add(PersonListEventDeletePerson(person: person));
              Navigator.of(context).pop();
            },
          ),
        ],
      ) : null,
    );
  }

}