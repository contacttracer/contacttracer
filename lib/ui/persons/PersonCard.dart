import 'package:contacttracer/models/Person.dart';
import 'package:contacttracer/ui/drawer/PersonalDataEditPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'PersonManagePage.dart';

class PersonCard extends StatelessWidget{
  /*
  Renders a Card to represent a person in a list. If the person is the owner of the app, it
  should be styled differently.
  */
  final Person person;
  final bool isOwner;
  PersonCard({@required this.person, this.isOwner=false }): assert(person != null);

  @override
  Widget build(BuildContext context) {
    var first_name = "???";
    var last_name = "";
    var phone = "Unknown";
    var email = "Unknown";
    var id = -1;
    if (person.first_name != null){
      first_name = person.first_name;
    }
    if (person.last_name != null){
      last_name = person.last_name;
    }
    if (person.phone != null){
      phone = person.phone;
    }
    if (person.email != null){
      email = person.email;
    }
    if (person.id != null){
      id = person.id;
    }

    List<Widget> _contactPossibilities(Person person){
      List<Widget> returnList = [];
      if (person.email != null){
        returnList.add(
          IconButton(
            icon: Icon(CupertinoIcons.mail),
            onPressed: () {
              print("Mail ${person.email}");
            },
          )
        );
      }
      if (person.phone != null){
        returnList.add(
            IconButton(
              icon: Icon(CupertinoIcons.phone),
              onPressed: () {
                print("Call ${person.phone}");
              },
            )
        );
      }
      return returnList;
    }

    return Card(
      shadowColor: isOwner? Theme.of(context).primaryColor : Colors.black,
      child: ListTile(
        enabled: true,
        leading: isOwner? Icon(CupertinoIcons.person_solid, color: Theme.of(context).primaryColor,) : person?.uuid != null ? Icon(CupertinoIcons.qrcode) : Icon(CupertinoIcons.person),
        title: Text("$first_name $last_name"),
        subtitle: Wrap(
          spacing: 2.0,
          runSpacing: 0.0,
          children: _contactPossibilities(person),
        ),
        onTap: (){
          if (isOwner){
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => PersonalDataEditPage(),
              ),
            );
          } else {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => PersonManagePage(person: person,),
              ),
            );
          }

        },
      ),
    );
  }


}