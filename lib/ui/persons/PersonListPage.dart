import 'package:contacttracer/bloc/OwnerBloc/bloc.dart';
import 'package:contacttracer/bloc/PersonListBloc/bloc.dart';
import 'package:contacttracer/ui/persons/PersonCard.dart';
import 'package:contacttracer/ui/persons/PersonManagePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';

import 'ScanPersonFromQrPage.dart';

class PersonListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(
          children: [
            BlocBuilder<OwnerBloc, OwnerState>(
                builder: (context, state){
                  Widget returnWidget = Container();
                  if (state is OwnerStateInitial) {
                    returnWidget = Center(
                      child: Text("Initial"),
                    ); //HomePage();
                  }
                  if (state is OwnerStateLoading) {
                    returnWidget = Center(
                      child: CircularProgressIndicator(),
                    ); //HomePage();
                  }
                  if (state is OwnerStateFailure){
                    returnWidget = Center(
                      child: Icon(CupertinoIcons.exclamationmark_triangle_fill),);
                  }
                  if (state is  OwnerStateDone){
                    returnWidget = PersonCard(person: state.owner, isOwner: true,);
                  }
                  return returnWidget;
                }
            ),
            BlocBuilder<PersonListBloc, PersonListState>(
              builder: (context, state){
                Widget returnWidget = Container();
                if (state is PersonListStateInitial) {
                  returnWidget = Center(
                    child: Text("Initial"),
                  ); //HomePage();
                }
                if (state is PersonListStateLoading) {
                  returnWidget = Center(
                    child: CircularProgressIndicator(),
                  ); //HomePage();
                }
                if (state is PersonListStateFailure){
                  returnWidget = Center(
                    child: Icon(CupertinoIcons.exclamationmark_triangle_fill),);
                }
                if (state is PersonListStateDone) {
                  returnWidget = ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: state.personList.length,
                      itemBuilder: (context, index) {
                        return PersonCard(person: state.personList[index]);
                      }
                  );
                }
                return returnWidget;
              },
            ),
          ],

        ),
      floatingActionButton: SpeedDial(
        marginRight: 18,
        marginBottom: 20,
        child: Icon(CupertinoIcons.add),
        // this is ignored if animatedIcon is non null
        // child: Icon(Icons.add),
        visible: true,
        // If true user is forced to close dial manually
        // by tapping main button and overlay is not rendered.
        closeManually: false,
        overlayOpacity: 0.5,
        heroTag: 'speed-dial-hero-tag',
        elevation: 8.0,
        shape: CircleBorder(),
        children: [
          SpeedDialChild(
            child: Icon(CupertinoIcons.person_add_solid),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => PersonManagePage(),
                ),
              )
            },
          ),
          SpeedDialChild(
            child: Icon(CupertinoIcons.qrcode),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ScanPersonFromQrPage(),
                ),
              )
            },
          ),
        ],
      ),
    );
  }
}


