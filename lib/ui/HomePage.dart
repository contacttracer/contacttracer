import 'package:contacttracer/ui/persons/PersonListPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

import 'display_qr/display_qr.dart';
import 'drawer/drawer.dart';
import 'encounters/MockEncounters.dart';
import 'locations/LocationListPage.dart';



class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            tabs: [
              Tab(icon: Icon(CupertinoIcons.person_2_square_stack)),
              Tab(icon: Icon(CupertinoIcons.person_solid)),
              Tab(icon: Icon(CupertinoIcons.location_solid)),
            ],
          ),
          title: Text('Contact Tracer'),
          actions: <Widget>[
            IconButton(
              icon: Icon(
                CupertinoIcons.qrcode_viewfinder,
                color: Colors.white,
              ),
              onPressed: () {
                displayQrDialog(context);
              },
            )
          ],
        ),
        drawer: PrimaryDrawer(),
        body: TabBarView(
          children: [
            MockEvents(),
            PersonListPage(),
            LocationListPage(),
          ],
        ),
      ),
    );
  }
}
