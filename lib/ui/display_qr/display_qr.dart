import 'dart:math';

import 'package:contacttracer/bloc/OwnerBloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'dart:convert';

displayQrDialog(context) {
  var size = 300.0;
  showDialog(
    context: context,
    builder: (_) => new CupertinoAlertDialog(
      title: Text("Contact Information"),
      content: Column(
        children: [
          Divider(),
          Text(
              "Scan this code to add the person holding this phone to your contacts or to log a meeting with this person."),
          Divider(),
          Container(
            width: size,
            height: size,
            child: BlocBuilder<OwnerBloc, OwnerState>(
              builder: (context, state) {
                Widget returnWidget = Container();

                if (state is OwnerStateInitial) {
                  returnWidget = Text("initial");
                }
                if (state is OwnerStateLoading) {
                  returnWidget = Center(
                    child: CircularProgressIndicator(),
                  );
                }
                if (state is OwnerStateFailure) {
                  returnWidget = Center(
                    child: Icon(CupertinoIcons.exclamationmark_triangle_fill),
                  );
                }
                if (state is OwnerStateDone) {
                  returnWidget = QrImage(
                    data: json.encode(state.owner.toMapQR()),
                    version: QrVersions.auto,
                    size: size,
                  );
                }
                return returnWidget;
              },
            ),
          )
        ],
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('Done'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        )
      ],
    ),
  );
}
