import 'package:contacttracer/models/Enocounter.dart';
import 'package:contacttracer/ui/encounters/EncounterManagePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


class EncounterCard extends StatelessWidget{
  /*
  Renders a Card to represent an encounter in a list.
  */
  final Encounter encounter;
  EncounterCard({@required this.encounter }): assert(encounter != null);

  @override
  Widget build(BuildContext context) {
    var name = "???";
    if (encounter?.location?.name != null){
      name = encounter.location.name;
    }
    if (encounter.name != null){
      name = encounter.name;
    }

    var description = "";
    if (encounter.description != null){
      description = encounter.description;
    }

    var id = -1;
    if (encounter.id != null){
      id = encounter.id;
    }

    final DateFormat dateFormatter = DateFormat('dd.MM.yyyy');
    final DateFormat timeFormatter = DateFormat('HH:mm');




    var timeString = "";

    var start_date = "";
    var end_date = "";
    var start_time = "";
    var end_time = "";
    if (encounter.start != null){
      start_date = dateFormatter.format(encounter.start);
      start_time = timeFormatter.format(encounter.start);
    }
    if (encounter.end != null){
      end_date = dateFormatter.format(encounter.end);
      end_time = timeFormatter.format(encounter.end);
    }

    if (encounter.start != null){
      if (encounter.end != null){
        if (start_date == end_date){
          if (start_time == end_time){
            timeString = "$start_date $start_time";
          } else{
            timeString = "$start_date $start_time - $end_time";
          }
        } else {
          timeString = "$start_date - $end_date";
        }
      } else {
        timeString = "$start_date $start_time";
      }
    } else {
      timeString = "Date and Time Unknown";
    }


    return Card(
      child: ListTile(
        leading: Icon(CupertinoIcons.person_2_square_stack),
        title: Text(name),
        subtitle: Text(timeString),
        trailing: Column(
          children: [Icon(CupertinoIcons.person_solid), Text(encounter?.personEncounters?.length != null ? "${encounter.personEncounters.length}" : "0")],
        ),
        onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => EncounterMangePage(encounter: encounter,),
            ),
          );
        },
      ),
    );
  }


}