import 'package:contacttracer/bloc/EncounterBloc/EncounterBloc.dart';
import 'package:contacttracer/bloc/EncounterBloc/bloc.dart';
import 'package:contacttracer/bloc/LocationBloc/bloc.dart';
import 'package:contacttracer/models/Enocounter.dart';
import 'package:contacttracer/models/Location.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:intl/intl.dart';

import 'SelectPersonsModal.dart';

class EncounterMangePage extends StatefulWidget {
  Encounter encounter;

  EncounterMangePage({this.encounter});

  @override
  State<StatefulWidget> createState() {
    DateTime initialDateTime = DateTime.now();
    initialDateTime =
        initialDateTime.subtract(Duration(minutes: initialDateTime.minute));

    Encounter cleanedEncounter = encounter;

    if (cleanedEncounter == null) {
      cleanedEncounter =
          new Encounter(start: initialDateTime, end: initialDateTime);
    }
    if (cleanedEncounter?.id == null){
      cleanedEncounter.start = initialDateTime;
    }
    if (cleanedEncounter?.id == null){
      cleanedEncounter.end = initialDateTime;
    }

    if (cleanedEncounter?.personEncounters == null) {
      cleanedEncounter.personEncounters = [];
    }

    return EncounterMangePageState(
      encounter: cleanedEncounter,
      initialDateTime: initialDateTime,
    );
  }
}

class EncounterMangePageState extends State<EncounterMangePage> {
  Encounter encounter;
  DateTime initialDateTime;

  List<int> idsOfPersonsAlreadySelected;

  EncounterMangePageState(
      {@required this.encounter, @required this.initialDateTime});

  Function updateParent(){

  }

  @override
  Widget build(BuildContext context) {
    final DateFormat formatter = DateFormat('dd.MM. HH:mm');

    if (idsOfPersonsAlreadySelected == null) {
      idsOfPersonsAlreadySelected = [];
    }

    encounter.personEncounters.forEach((element) {
      if (idsOfPersonsAlreadySelected.contains(element.person.id)) {
        // nothing to do
      } else {
        idsOfPersonsAlreadySelected.add(element.person.id);
      }
    });
    print(idsOfPersonsAlreadySelected);
    return Scaffold(
      appBar: AppBar(
        title: encounter.id == null
            ? Text("Add new Encounter")
            : Text("Update Encounter"),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              Container(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Name',
                  ),
                  enabled: true,
                  controller: new TextEditingController.fromValue(
                    new TextEditingValue(
                      text: encounter.name != null ? encounter.name : "",
                      selection: new TextSelection.collapsed(
                          offset: encounter.name != null
                              ? encounter.name.length
                              : 0),
                    ),
                  ),
                  onChanged: (text) {
                    encounter.name = text;
                    if (encounter.id != null) {
                      BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                    }
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.all(5.0),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Description',
                  ),
                  enabled: true,
                  controller: new TextEditingController.fromValue(
                    new TextEditingValue(
                      text: encounter.description != null
                          ? encounter.description
                          : "",
                      selection: new TextSelection.collapsed(
                          offset: encounter.description != null
                              ? encounter.description.length
                              : 0),
                    ),
                  ),
                  onChanged: (text) {
                    encounter.description = text;
                    if (encounter.id != null) {
                      BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                    }
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.all(3),
                child: Card(
                  child: ListTile(
                    leading: Icon(CupertinoIcons.location_solid),
                    title: Text(encounter?.location?.name != null
                        ? encounter.location.name
                        : ""),
                    subtitle: Text(encounter?.location?.description != null
                        ? encounter.location.description
                        : ""),
                    onTap: () {
                      showCupertinoModalPopup(
                        context: context,
                        builder: (context) {
                          return Container(
                            height: MediaQuery.of(context).size.height * 0.4,
                            child: BlocBuilder<LocationBloc, LocationState>(
                              builder: (context, state) {
                                Widget returnWidget = Container();
                                if (state is LocationStateInitial) {
                                  returnWidget = Center(
                                    child: Text("Initial"),
                                  ); //HomePage();
                                }
                                if (state is LocationStateLoading) {
                                  returnWidget = Center(
                                    child: CircularProgressIndicator(),
                                  ); //HomePage();
                                }
                                if (state is LocationStateFailure) {
                                  returnWidget = Center(
                                    child: Icon(CupertinoIcons
                                        .exclamationmark_triangle_fill),
                                  );
                                }
                                if (state is LocationStateDone) {
                                  List<Location> locationList = [null];
                                  locationList.addAll(state.locations);

                                  returnWidget = CupertinoPicker.builder(
                                      backgroundColor: Colors.white,
                                      childCount: locationList.length,
                                      itemExtent: 30.0,
                                      useMagnifier: true,
                                      magnification: 1.2,
                                      onSelectedItemChanged: (index) {
                                        setState(() {
                                          encounter.location =
                                              locationList[index];
                                          if (encounter.id != null) {
                                            BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                                          }
                                        });
                                      },
                                      itemBuilder: (context, index) {
                                        Widget returnWidget = Text(
                                            locationList[index]?.name != null
                                                ? locationList[index].name
                                                : "?");
                                        if (index == 0) {
                                          returnWidget = Text("---");
                                        }
                                        return returnWidget;
                                      });
                                }
                                return returnWidget;
                              },
                            ),
                          );
                        },
                      );
                    },
                  ),
                ),
              ),
              Row(
                children: [
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(left: 3),
                      child: Card(
                        child: ListTile(
                          title: Text("Start"),
                          trailing: IconButton(
                              icon: Icon(CupertinoIcons.clear),
                              onPressed: () {
                                setState(() {
                                  encounter.start = null;
                                  if (encounter.id != null) {
                                    BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                                  }
                                });
                              }),
                          subtitle: Text(encounter.start != null
                              ? formatter.format(encounter.start)
                              : ""),
                          onTap: () {
                            showCupertinoModalPopup(
                              context: context,
                              builder: (context) {
                                return Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.4,
                                  child: CupertinoDatePicker(
                                      minuteInterval: 5,
                                      initialDateTime: encounter.start != null
                                          ? encounter.start
                                          : initialDateTime,
                                      use24hFormat: true,
                                      backgroundColor: Colors.white,
                                      onDateTimeChanged: (datetime) {
                                        setState(() {
                                          encounter.start = datetime;
                                          if (encounter.id != null) {
                                            BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                                          }
                                        });
                                      }),
                                );
                              },
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.only(right: 3),
                      child: Card(
                        child: ListTile(
                          title: Text("End"),
                          trailing: IconButton(
                              icon: Icon(CupertinoIcons.clear),
                              onPressed: () {
                                setState(() {
                                  encounter.end = null;
                                  if (encounter.id != null) {
                                    BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                                  }
                                });
                              }),
                          subtitle: Text(encounter.end != null
                              ? formatter.format(encounter.end)
                              : ""),
                          onTap: () {
                            showCupertinoModalPopup(
                              context: context,
                              builder: (context) {
                                return Container(
                                  height:
                                      MediaQuery.of(context).size.height * 0.4,
                                  child: CupertinoDatePicker(
                                      initialDateTime: encounter.end != null
                                          ? encounter.end
                                          : initialDateTime,
                                      use24hFormat: true,
                                      backgroundColor: Colors.white,
                                      onDateTimeChanged: (datetime) {
                                        setState(() {
                                          encounter.end = datetime;
                                          if (encounter.id != null) {
                                            BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                                          }
                                        });
                                      }),
                                );
                              },
                            );
                          },
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
          ButtonBar(
            buttonPadding: EdgeInsets.all(5.0),
            alignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                child: Text(encounter?.id != null ? "Done" : "Save"),
                onPressed: () {
                    BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                    Navigator.of(context).pop();
                },
              ),
            ],
          ),
          ButtonBar(
            buttonPadding: EdgeInsets.all(5.0),
            alignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                child: Text("Select Persons"),
                onPressed: () {
                  showCupertinoDialog(
                      context: context,
                      builder: (context) {
                        print("encounter.id ${encounter.id}");
                        return SelectPersonsModal(encounter: encounter, parent: this, idsOfPersonsAlreadySelected: idsOfPersonsAlreadySelected,);
                      });
                },
              ),
            ],
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: encounter?.personEncounters?.length != null
                ? encounter.personEncounters.length
                : 0,
            itemBuilder: (context, index) {
              String first_name =
                  encounter.personEncounters[index]?.person?.first_name != null
                      ? encounter.personEncounters[index].person.first_name
                      : "";
              String last_name =
                  encounter.personEncounters[index]?.person?.last_name != null
                      ? encounter.personEncounters[index].person.last_name
                      : "";
              return Card(
                child: ListTile(
                  onTap: (){
                    if (encounter.personEncounters[index]?.mask != null
                        ? encounter.personEncounters[index].mask
                        : false){
                      setState(() {
                        encounter.personEncounters[index].mask = false;
                      });
                    } else {
                      setState(() {
                        encounter.personEncounters[index].mask = true;
                      });
                    }
                    if (encounter.id != null) {
                      BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                    }
                  },
                  onLongPress: (){},
                  title: Text("$first_name $last_name"),
                  trailing: Container(
                    width: 100,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Icon(Icons.masks),
                        CupertinoSwitch(
                          activeColor: Theme.of(context).primaryColor,
                          value: encounter.personEncounters[index]?.mask != null
                              ? encounter.personEncounters[index].mask
                              : false,
                          onChanged: (bool value) {
                            setState(() {
                              encounter.personEncounters[index].mask = value;
                              if (encounter.id != null) {
                                BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                              }
                            });
                          },
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
      floatingActionButton: encounter.id != null
          ? SpeedDial(
              marginRight: 18,
              marginBottom: 20,
              animatedIcon: AnimatedIcons.menu_close,
              // this is ignored if animatedIcon is non null
              // child: Icon(Icons.add),
              visible: true,
              // If true user is forced to close dial manually
              // by tapping main button and overlay is not rendered.
              closeManually: false,
              overlayOpacity: 0.5,
              heroTag: 'speed-dial-hero-tag',
              elevation: 8.0,
              shape: CircleBorder(),
              children: [
                SpeedDialChild(
                  child: Icon(CupertinoIcons.delete),
                  backgroundColor: Colors.redAccent,
                  onTap: () {
                      BlocProvider.of<EncounterBloc>(context)..add(EncounterEventDelete(encounter: encounter));
                    Navigator.of(context).pop();
                  },
                ),
              ],
            )
          : null,
    );
  }
}
