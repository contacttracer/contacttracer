import 'package:contacttracer/bloc/EncounterBloc/bloc.dart';
import 'package:contacttracer/bloc/PersonListBloc/bloc.dart';
import 'package:contacttracer/models/Enocounter.dart';
import 'package:contacttracer/models/PersonEncounter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'EncounterManagePage.dart';

class SelectPersonsModal extends StatefulWidget {

  Encounter encounter;
  EncounterMangePageState parent;
  List<int> idsOfPersonsAlreadySelected;

  SelectPersonsModal(
      {@required this.encounter, @required this.parent, @required this.idsOfPersonsAlreadySelected});


  @override
  State<StatefulWidget> createState() {
    return SelectPersonsModalState(encounter: encounter, parent: parent, idsOfPersonsAlreadySelected: idsOfPersonsAlreadySelected);
  }

}

 class SelectPersonsModalState extends State<SelectPersonsModal> {

   Encounter encounter;
   EncounterMangePageState parent;
   List<int> idsOfPersonsAlreadySelected;

   SelectPersonsModalState(
       {@required this.encounter, @required this.parent, @required this.idsOfPersonsAlreadySelected});



   @override
  Widget build(BuildContext context) {


    return CupertinoAlertDialog(
      title: Text("Select Persons ${encounter?.id}"),
      content: BlocBuilder<PersonListBloc, PersonListState>(
        builder: (context, state) {
          Widget returnWidget = Container();
          if (state is PersonListStateInitial) {
            returnWidget = Center(
              child: Text("Initial"),
            ); //HomePage();
          }
          if (state is PersonListStateLoading) {
            returnWidget = Center(
              child: CupertinoActivityIndicator(),
            ); //HomePage();
          }
          if (state is PersonListStateFailure) {
            returnWidget = Center(
              child: Icon(CupertinoIcons
                  .exclamationmark_triangle_fill),
            );
          }
          if (state is PersonListStateDone) {
            returnWidget = Container(
              height:
              MediaQuery.of(context).size.height * 0.6,
              width:
              MediaQuery.of(context).size.width * 0.8,
              child: ListView.builder(
                  itemCount: state.personList.length,
                  itemBuilder: (context, index) {
                    String first_name = state
                        .personList[index]
                        ?.first_name !=
                        null
                        ? state.personList[index].first_name
                        : "";
                    String last_name = state
                        .personList[index]
                        ?.last_name !=
                        null
                        ? state.personList[index].last_name
                        : "";

                    return Card(
                      child: ListTile(
                        title:
                        Text("$first_name $last_name ${state.personList[index]?.id}"),
                        leading: Checkbox(
                          value: idsOfPersonsAlreadySelected
                              .contains(state
                              .personList[index].id),
                          onChanged: (bool) {
                            if (bool) {
                              if (idsOfPersonsAlreadySelected
                                  .contains(state
                                      .personList[
                                  index]
                                      .id)) {
                                // id is in the list and should be, everything ok
                              } else {
                                setState(() {
                                  this.parent.setState(() {
                                    idsOfPersonsAlreadySelected
                                        .add(state
                                        .personList[index]
                                        .id);
                                    encounter.personEncounters
                                        .add(PersonEncounter(
                                      encounter_id: encounter?.id,
                                        person: state
                                            .personList[
                                        index]));
                                  });
                                });
                                if (encounter.id != null) {
                                  BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                                }

                              }
                            } else {
                              if (idsOfPersonsAlreadySelected
                                  .contains(state
                                      .personList[
                                  index]
                                      .id)) {
                                setState(() {
                                  this.parent.setState(() {
                                    idsOfPersonsAlreadySelected
                                        .remove(state
                                        .personList[index]
                                        .id);
                                    encounter.personEncounters
                                        .removeWhere((item) => item.person.id == state
                                        .personList[
                                    index]
                                        .id);
                                  });
                                });
                                if (encounter.id != null) {
                                  BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
                                }
                              } else {
                                // id is not the list and shouldn't  be, everything ok
                              }
                            }
                          },
                        ),
                      ),
                    );
                  }),
            );
          }
          return returnWidget;
        },
      ),
      actions: [
        CupertinoButton(
          child: Text("Done"),
          onPressed: () {
            if (encounter.id != null) {
              BlocProvider.of<EncounterBloc>(context)..add(EncounterEventSave(encounter: encounter));
            }
            Navigator.of(context).pop();
          },
        ),
      ],
    );


  }


}