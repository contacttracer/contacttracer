import 'package:contacttracer/bloc/EncounterBloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'EncounterCard.dart';
import 'EncounterManagePage.dart';

class MockEvents extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<EncounterBloc, EncounterState>(
        builder: (context, state){
          Widget returnWidget = Container();
          if (state is EncounterStateInitial) {
            returnWidget = Center(
              child: Text("Initial"),
            ); //HomePage();
          }
          if (state is EncounterStateLoading) {
            returnWidget = Center(
              child: CircularProgressIndicator(),
            ); //HomePage();
          }
          if (state is EncounterStateFailure){
            returnWidget = Center(
              child: Icon(CupertinoIcons.exclamationmark_triangle_fill),);
          }
          if (state is EncounterStateDone){
            if (state.encounters.length == 0){
              returnWidget = Center(child: Text("no Encounters"),);
            } else{
              returnWidget = ListView.builder(
                  itemCount: state.encounters.length,
                  itemBuilder: (context, index){
                    return EncounterCard(encounter: state.encounters[index],);
                  }
              );
            }
          }

          return returnWidget;
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(CupertinoIcons.add),
        onPressed: (){
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => EncounterMangePage(),
            ),
          );
        },
      )
    );
  }
}