import 'package:contacttracer/models/Location.dart';

import 'PersonEncounter.dart';

class Encounter{
  int id;
  String name;
  String description;
  Location location;
  List<PersonEncounter> personEncounters;
  DateTime start;
  DateTime end;

  Encounter({
    this.id,
    this.name,
    this.description,
    this.location,
    this.personEncounters,
    this.start,
    this.end,
  });

  factory Encounter.fromMap(Map<String, dynamic> data) => new Encounter(
    id: data["id"],
    name: data["name"],
    description: data["description"],
    start: data["start"] != null ? DateTime.fromMillisecondsSinceEpoch(data["start"]) : null,
    end: data["end"] != null ? DateTime.fromMillisecondsSinceEpoch(data["end"]) : null,
  );


  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,
    "description": description,
    "location_id": location?.id,
    "start": start?.millisecondsSinceEpoch,
    "end": end?.millisecondsSinceEpoch,
  };

}