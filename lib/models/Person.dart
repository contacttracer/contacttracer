class Person{
  int id;
  String uuid;
  String first_name;
  String last_name;
  String phone;
  String email;

  Person({
    this.id,
    this.uuid,
    this.first_name,
    this.last_name,
    this.email,
    this.phone,
  });

  factory Person.fromMap(Map<String, dynamic> data) => new Person(
    id: data["id"],
    uuid: data["uuid"],
    first_name: data["first_name"],
    last_name: data["last_name"],
    email: data["email"],
    phone: data["phone"],
  );


  Map<String, dynamic> toMap() => {
    "id": id,
    "uuid": uuid,
    "first_name": first_name,
    "last_name": last_name,
    "email": email,
    "phone": phone,
  };


  Map<String, dynamic> toMapQR() => {
    "uuid": uuid,
    "first_name": first_name,
    "last_name": last_name,
    "email": email,
    "phone": phone,
  };

  @override
  String toString() {
    return 'Person{id: $id, name: $first_name $last_name}';
  }
}