
import 'Person.dart';

class PersonEncounter{
  int id;
  Person person;
  int encounter_id;
  bool mask;
  String notes;

  PersonEncounter({
    this.id,
    this.person,
    this.encounter_id,
    this.mask,
    this.notes,
  });

  factory PersonEncounter.fromMap(Map<String, dynamic> data) => new PersonEncounter(
    id: data["id"],
    encounter_id: data["encounter_id"],
    mask: data["mask"] == 1 ? true : false,
    notes: data["notes"],
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "person_id": person?.id,
    "encounter_id": encounter_id,
    "mask": mask != null ? mask ? 1: 0 : 0,
    "notes": notes,
  };

}