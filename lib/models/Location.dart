class Location{
  int id;
  String name;
  String description;

  Location({
    this.id,
    this.name,
    this.description,
  });

  factory Location.fromMap(Map<String, dynamic> data) => new Location(
    id: data["id"],
    name: data["name"],
    description: data["description"],
  );


  Map<String, dynamic> toMap() => {
    "id": id,
    "name": name,
    "description": description,
  };

}