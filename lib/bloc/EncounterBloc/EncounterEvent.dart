import 'package:contacttracer/models/Enocounter.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class EncounterEvent extends Equatable{
  const EncounterEvent();

  @override
  List<Object> get props => [];
}

class EncounterEventRefresh extends EncounterEvent{}
class EncounterEventSave extends EncounterEvent{
  Encounter encounter;

  EncounterEventSave({@required this.encounter});

  @override
  List<Object> get props => [encounter];
}


class EncounterEventDelete extends EncounterEvent{
  Encounter encounter;

  EncounterEventDelete({@required this.encounter});

  @override
  List<Object> get props => [encounter];
}
