import 'package:contacttracer/bloc/EncounterBloc/EncounterState.dart';
import 'package:contacttracer/db/DBProvider.dart';
import 'package:contacttracer/models/Enocounter.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'EncounterEvent.dart';

class EncounterBloc extends Bloc<EncounterEvent, EncounterState>{
  final DBProvider dbprovider;

  EncounterBloc({
    @required this.dbprovider
  }): super(EncounterStateInitial());

  @override
  Stream<EncounterState> mapEventToState(EncounterEvent event) async* {
    if (event is EncounterEventRefresh){
      yield EncounterStateLoading();
      List<Encounter> encounter = await dbprovider.getAllEncounters();

      try {
        yield EncounterStateDone(encounters: encounter);
      } catch (exception) {
        yield EncounterStateFailure(error: exception.toString());
        print(exception.toString());
      }
    }

    if (event is EncounterEventSave){
      //  Update existing person if id. If no id, save as new person unless this
      //  data already exists.
      yield EncounterStateLoading();
      print("Save: ${event.encounter.toMap()}");
      print("PersonEncounters: ${event.encounter?.personEncounters?.length}");

      if (event.encounter.id != null){
        try{
          await dbprovider.updateEncounter(event.encounter);
          this.add(EncounterEventRefresh());

        } catch (exception) {
          print(exception.toString());
        }
      } else {
        // new person
        try{
          await dbprovider.insertEncounter(event.encounter);
          this.add(EncounterEventRefresh());
        } catch (exception) {
          print(exception.toString());
        }
      }
    }

    if (event is EncounterEventDelete){
      //  Delete the person
      if (event.encounter.id == null){
        //nothing to do...
      } else {
        // location already in database
        try{
          yield EncounterStateLoading();
          await dbprovider.deleteEncounter(event.encounter);
          this.add(EncounterEventRefresh());

        } catch (exception) {
          print(exception.toString());
        }
      }
    }


  }





}