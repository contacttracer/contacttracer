import 'package:contacttracer/models/Enocounter.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
abstract class EncounterState extends Equatable{
  const EncounterState();

  @override
  List<Object> get props => [];
}


class EncounterStateInitial extends EncounterState{}

class EncounterStateLoading extends EncounterState{}

class EncounterStateDone extends EncounterState{
  final List<Encounter> encounters;
  EncounterStateDone({@required this.encounters});

  @override
  List<Object> get props => [encounters];
}

class EncounterStateFailure extends EncounterState{
  final String error;

  const EncounterStateFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'EncounterStateFailure { error: $error }';
}