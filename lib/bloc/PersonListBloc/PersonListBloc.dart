import 'dart:convert';

import 'package:contacttracer/bloc/PersonListBloc/PersonListEvent.dart';
import 'package:contacttracer/bloc/PersonListBloc/PersonListState.dart';
import 'package:contacttracer/db/DBProvider.dart';
import 'package:contacttracer/models/Person.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

class PersonListBloc extends Bloc<PersonListEvent, PersonListState>{
  final DBProvider dbprovider;

  PersonListBloc({
    @required this.dbprovider
  }): super(PersonListStateInitial());

  @override
  Stream<PersonListState> mapEventToState(PersonListEvent event) async* {
    if (event is PersonListEventDeletePerson){
      //  Delete the person
      if (event.person.id == null){
        //nothing to do...
      } else {
        // person already in database
        try{
          yield PersonListStateLoading();
          await dbprovider.deletePerson(event.person);
          this.add(PersonListEventRefresh());

        } catch (exception) {
          print(exception.toString());
        }
      }
    }


    if (event is PersonListEventSavePerson){
      //  Update existing person if id. If no id, save as new person unless this
      //  data already exists.
      if (event.person.id != null){
        try{
          yield PersonListStateLoading();
          await dbprovider.updatePerson(event.person);
          this.add(PersonListEventRefresh());

        } catch (exception) {
          print(exception.toString());
        }
      } else {
        // new person
        try{
          yield PersonListStateLoading();
          await dbprovider.insertPerson(event.person);
          this.add(PersonListEventRefresh());

        } catch (exception) {
          print(exception.toString());
        }
      }
    }

    if (event is PersonListEventSavePersonFromQr) {
      Map data = jsonDecode(event.data);
      Person person = Person.fromMap(data);
      Person personInDb = await dbprovider.getPersonUuid(person.uuid);

      if (personInDb != null){
        //  update the person with the new data
        person.id = personInDb.id;
        await dbprovider.updatePerson(person);
        this.add(PersonListEventRefresh());
      } else {
        // insert a new person with the data
        await dbprovider.insertPerson(person);
        this.add(PersonListEventRefresh());
      }

    }

    if (event is PersonListEventRefresh){
      yield PersonListStateLoading();
      try {
        List<Person> persons = await dbprovider.getAllPersons();
        yield PersonListStateDone(personList: persons);
      } catch (exception) {
        yield PersonListStateFailure(error: exception.toString());
      }
    }
  }

}