import 'package:contacttracer/models/Person.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PersonListEvent extends Equatable{
  const PersonListEvent();

  @override
  List<Object> get props => [];
}

class PersonListEventRefresh extends PersonListEvent{}

class PersonListEventSavePerson extends PersonListEvent{
  Person person;

  PersonListEventSavePerson({
    @required this.person,
  });

  @override
  List<Object> get props => [person];
}

class PersonListEventSavePersonFromQr extends PersonListEvent{
  String data;
  PersonListEventSavePersonFromQr({
    @required this.data,
  });

  @override
  List<Object> get props => [data];
}

class PersonListEventDeletePerson extends PersonListEvent{
  Person person;

  PersonListEventDeletePerson({
    @required this.person,
  });

  @override
  List<Object> get props => [person];
}