import 'package:contacttracer/models/Person.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class PersonListState extends Equatable{
  const PersonListState();
  @override
  List<Object> get props => [];
}


class PersonListStateInitial extends PersonListState{}

class PersonListStateLoading extends PersonListState{}

class PersonListStateDone extends PersonListState{
  final List<Person> personList;
  PersonListStateDone({@required this.personList});

  @override
  List<Object> get props => [personList];
}

class PersonListStateFailure extends PersonListState{
  final String error;

  const PersonListStateFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'PersonListStateFailure { error: $error }';
}