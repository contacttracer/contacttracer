import 'package:contacttracer/models/Person.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
abstract class OwnerEvent extends Equatable{
  const OwnerEvent();

  @override
  List<Object> get props => [];
}

class OwnerEventRefresh extends OwnerEvent{}

class OwnerEventSave extends OwnerEvent{

  Person owner;

  OwnerEventSave({
    @required this.owner,
  });

  @override
  List<Object> get props => [owner];
}