import 'package:contacttracer/bloc/OwnerBloc/OwnerState.dart';
import 'package:contacttracer/db/DBProvider.dart';
import 'package:contacttracer/models/Person.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';
import 'OwnerEvent.dart';

class OwnerBloc extends Bloc<OwnerEvent, OwnerState>{
  final DBProvider dbprovider;

  OwnerBloc({
    @required this.dbprovider
  }): super(OwnerStateInitial());

  @override
  Stream<OwnerState> mapEventToState(OwnerEvent event) async* {
    if (event is OwnerEventRefresh){
      yield OwnerStateLoading();
      try {
        Person person = await dbprovider.getOwner();
        yield OwnerStateDone(owner: person);
      } catch (exception) {
        yield OwnerStateFailure(error: exception.toString());
      }
    }
    if (event is OwnerEventSave){
      try{
        await dbprovider.saveOwner(event.owner);
        this.add(OwnerEventRefresh());
      } catch (exception){
        print(exception.toString());
        yield OwnerStateFailure(error: exception.toString());
      }
    }

  }

}