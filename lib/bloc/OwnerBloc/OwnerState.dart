import 'package:contacttracer/models/Person.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class OwnerState extends Equatable{
  const OwnerState();
  @override
  List<Object> get props => [];
}


class OwnerStateInitial extends OwnerState{}

class OwnerStateLoading extends OwnerState{}

class OwnerStateDone extends OwnerState{
  final Person owner;
  OwnerStateDone({@required this.owner});

  @override
  List<Object> get props => [owner];
}

class OwnerStateFailure extends OwnerState{
  final String error;

  const OwnerStateFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'OwnerStateFailure { error: $error }';
}