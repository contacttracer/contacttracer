import 'package:contacttracer/models/Location.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LocationEvent extends Equatable{
  const LocationEvent();

  @override
  List<Object> get props => [];
}

class LocationEventRefresh extends LocationEvent{}

class LocationEventSave extends LocationEvent{

  Location location;

  LocationEventSave({
    @required this.location,
  });

  @override
  List<Object> get props => [location];
}


class LocationEventDelete extends LocationEvent{

  Location location;

  LocationEventDelete({
    @required this.location,
  });

  @override
  List<Object> get props => [location];
}