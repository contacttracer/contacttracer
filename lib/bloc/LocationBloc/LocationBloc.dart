
import 'package:contacttracer/bloc/LocationBloc/LocationEvent.dart';
import 'package:contacttracer/bloc/LocationBloc/LocationState.dart';
import 'package:contacttracer/db/DBProvider.dart';
import 'package:contacttracer/models/Location.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';


class LocationBloc extends Bloc<LocationEvent, LocationState>{
  final DBProvider dbprovider;

  LocationBloc({
    @required this.dbprovider
  }): super(LocationStateInitial());

  @override
  Stream<LocationState> mapEventToState(LocationEvent event) async* {

    if (event is LocationEventDelete){
      //  Delete the person
      if (event.location.id == null){
        //nothing to do...
      } else {
        // location already in database
        try{
          yield LocationStateLoading();
          await dbprovider.deleteLocation(event.location);
          this.add(LocationEventRefresh());

        } catch (exception) {
          print(exception.toString());
        }
      }
    }

    if (event is LocationEventRefresh){
      yield LocationStateLoading();
      try {
        List<Location> locations  = await dbprovider.getAllLocations();
        yield LocationStateDone(locations: locations);
      } catch (exception) {
        yield LocationStateFailure(error: exception.toString());
      }
    }

    if (event is LocationEventSave){
      //  Update existing person if id. If no id, save as new person unless this
      //  data already exists.
      yield LocationStateLoading();

      if (event.location.id != null){
        try{
          await dbprovider.updateLocation(event.location);
          this.add(LocationEventRefresh());

        } catch (exception) {
          print(exception.toString());
        }
      } else {
        // new person
        try{
          await dbprovider.insertLocation(event.location);
          this.add(LocationEventRefresh());
        } catch (exception) {
          print(exception.toString());
        }
      }
    }


  }

}