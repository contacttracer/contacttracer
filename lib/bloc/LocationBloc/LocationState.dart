import 'package:contacttracer/models/Location.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

abstract class LocationState extends Equatable{
  const LocationState();
  @override
  List<Object> get props => [];
}


class LocationStateInitial extends LocationState{}

class LocationStateLoading extends LocationState{}

class LocationStateDone extends LocationState{
  final List<Location> locations;
  LocationStateDone({@required this.locations});

  @override
  List<Object> get props => [locations];
}

class LocationStateFailure extends LocationState{
  final String error;

  const LocationStateFailure({@required this.error});

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'LocationStateFailure { error: $error }';
}