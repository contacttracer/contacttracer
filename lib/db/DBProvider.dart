import 'dart:async';
import 'dart:io';

import 'package:contacttracer/models/Enocounter.dart';
import 'package:contacttracer/models/Location.dart';
import 'package:contacttracer/models/Person.dart';
import 'package:contacttracer/models/PersonEncounter.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:uuid/uuid.dart';
import 'package:uuid/uuid_util.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();
  DBProvider();

  Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // if _database is null we instantiate it
    _database = await initDB();
    return _database;
  }

  deleteDB() async {
    final db = await database;

    // person
    await db.execute('''
      DROP TABLE IF EXISTS person;
      ''');
    await db.execute('''
      CREATE TABLE person(
          id INTEGER PRIMARY KEY NOT NULL,
          uuid TEXT,
          first_name TEXT,
          last_name TEXT,
          phone TEXT,
          email TEXT,
          CONSTRAINT idUniquePerson UNIQUE (id),
          CONSTRAINT uuidUniquePerson UNIQUE (uuid)
        );
      ''');

    // owner
    await db.execute('''
      DROP TABLE IF EXISTS owner;
      ''');
    await db.execute('''
            CREATE TABLE owner(
            id INTEGER PRIMARY KEY NOT NULL,
            uuid TEXT,
            first_name TEXT,
            last_name TEXT,
            phone TEXT,
            email TEXT,
            CONSTRAINT idUniquePerson UNIQUE (id),
            CONSTRAINT uuidUniquePerson UNIQUE (uuid)
            );
          ''');


    // location
    await db.execute('''
      DROP TABLE IF EXISTS location;
        ''');
    await db.execute('''
      CREATE TABLE location(
        id INTEGER PRIMARY KEY NOT NULL,
        name TEXT,
        description TEXT,
        CONSTRAINT idUniqueLocation UNIQUE (id)
        );
        ''');

    // encounter
    db.execute('''
      DROP TABLE IF EXISTS encounter;
        ''');
    db.execute('''
      CREATE TABLE encounter(
        id INTEGER PRIMARY KEY NOT NULL,
        name TEXT,
        description TEXT,
        location_id INTEGER,
        start INTEGER,
        end INTEGER,
        CONSTRAINT idUniqueEncounter UNIQUE (id)
        );
        ''');

    // event
    db.execute('''
      DROP TABLE IF EXISTS person_encounter;
        ''');
    db.execute('''
      CREATE TABLE person_encounter(
        id INTEGER PRIMARY KEY NOT NULL,
        person_id INTEGER,
        encounter_id INTEGER,
        mask INTEGER,
        notes STRING,
        CONSTRAINT idUniquePersonEncounter UNIQUE (id)
        );
        ''');

  }

  void deleteDBFile() async {
    final db = await database;
    await db.close();

    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    try {
      String path = join(documentsDirectory.path, "contacttracer.db");
      File databaseFile = new File(path);
      databaseFile.delete(recursive: false);
    } catch (e) {
      print("===deleteDB===: Database already gone");
    }
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "contacttracer.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
          // person
          await db.execute('''
            CREATE TABLE person(
            id INTEGER PRIMARY KEY NOT NULL,
            uuid TEXT,
            first_name TEXT,
            last_name TEXT,
            phone TEXT,
            email TEXT,
            CONSTRAINT idUniquePerson UNIQUE (id)
            );
          ''');

          await db.execute('''
            CREATE TABLE owner(
            id INTEGER PRIMARY KEY NOT NULL,
            uuid TEXT,
            first_name TEXT,
            last_name TEXT,
            phone TEXT,
            email TEXT,
            CONSTRAINT idUniquePerson UNIQUE (id)
            );
          ''');

          // location
          await db.execute('''
            CREATE TABLE location(
            id INTEGER PRIMARY KEY NOT NULL,
            name TEXT,
            description TEXT,
            CONSTRAINT idUniqueLocation UNIQUE (id)
            );
          ''');

          // encounter
          await db.execute('''
            CREATE TABLE encounter(
            id INTEGER PRIMARY KEY NOT NULL,
            name TEXT,
            description TEXT,
            location_id INTEGER,
            start INTEGER,
            end INTEGER,
            CONSTRAINT idUniqueEncounter UNIQUE (id)
            );
          ''');

          // event
          await db.execute('''
            CREATE TABLE person_encounter(
            id INTEGER PRIMARY KEY NOT NULL,
            person_id INTEGER,
            encounter_id INTEGER,
            mask INTEGER,
            notes STRING,
            CONSTRAINT idUniquePersonEncounter UNIQUE (id)
            );
          ''');
        });
  }

  // #################################################
  // person
  Future<int> insertPerson(Person person) async {
    // use this method for getting picks via the api
    // because of using toMapApi, isFavourite is always false.
    if (person.first_name == ""){
      person.first_name = null;
    }
    if (person.last_name == ""){
      person.last_name = null;
    }
    if (person.email == ""){
      person.email = null;
    }
    if (person.phone == ""){
      person.phone = null;
    }
    final db = await database;
    int id = await db.insert(
      'person',
      person.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }



  Future<int> updatePerson(Person person) async {

    if (person.first_name == ""){
      person.first_name = null;
    }
    if (person.last_name == ""){
      person.last_name = null;
    }
    if (person.email == ""){
      person.email = null;
    }
    if (person.phone == ""){
      person.phone = null;
    }

    final db = await database;
    int id = await db.update(
      'person',
      person.toMap(),
      where: "id = ?",
      whereArgs: [person.id],
    );
    return id;
  }


  Future<List<Person>> getAllPersons() async {
    final db = await database;
    var res = await db.query("person", orderBy: "last_name ASC, first_name ASC");
    List<Person> list = res.isNotEmpty ? res.map((c) => Person.fromMap(c)).toList() : [];
    return list;
  }

  Future<Person> getPerson(int id) async {
    final db = await database;
    var res = await db.query("person", where: "id = ?", whereArgs: [id]);
    List<Person> list = res.isNotEmpty ? res.map((c) => Person.fromMap(c)).toList() : [];
    Person singlePerson;
    if (list.length > 0){
      singlePerson = list[0];
    }
    return singlePerson;
  }

  Future<Person> getPersonUuid(String uuid) async {
    final db = await database;
    var res = await db.query("person", where: "uuid = ?", whereArgs: [uuid]);
    List<Person> list = res.isNotEmpty ? res.map((c) => Person.fromMap(c)).toList() : [];
    Person singlePerson;
    if (list.length > 0){
      singlePerson = list[0];
    }
    return singlePerson;
  }


  deletePerson(Person person) async {
    //todo: relations
    final db = await database;
    var res = await db.delete("person", where: "id = ?", whereArgs: [person.id]);
    return res;
  }

  void printPersonData() async {
    final db = await database;
    var res = await db.query("person");
  }


  Future<int> saveOwner(Person person) async {
    // use this method for getting picks via the api
    // because of using toMapApi, isFavourite is always false.
    if (person.first_name == ""){
      person.first_name = null;
    }
    if (person.last_name == ""){
      person.last_name = null;
    }
    if (person.email == ""){
      person.email = null;
    }
    if (person.phone == ""){
      person.phone = null;
    }
    person.id = 1;
    final db = await database;
    int id = await db.insert(
      'owner',
      person.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }


  Future<Person> getOwner() async {
    var uuid = Uuid();

    final db = await database;
    var res = await db.query(
      'owner',
    );
    List<Person> list = res.isNotEmpty ? res.map((c) => Person.fromMap(c)).toList() : [];
    if (list.length > 0){
      return list.first;
    } else{
      return Person(id: 1,uuid: uuid.v4(options: {'rng': UuidUtil.cryptoRNG}));
    }
  }


  // #################################################
  // location

  Future<List<Location>> getAllLocations() async {
    final db = await database;
    var res = await db.query("location", orderBy: "name ASC");
    List<Location> list = res.isNotEmpty ? res.map((c) => Location.fromMap(c)).toList() : [];
    return list;
  }


  Future<int> insertLocation(Location location) async {
    // use this method for getting picks via the api
    // because of using toMapApi, isFavourite is always false.
    if (location.name == ""){
      location.name = null;
    }
    if (location.description == ""){
      location.description = null;
    }
    final db = await database;
    int id = await db.insert(
      'location',
      location.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }

  Future<int> updateLocation(Location location) async {
    if (location.name == ""){
      location.name = null;
    }
    if (location.description == ""){
      location.description = null;
    }

    final db = await database;
    int id = await db.update(
      'location',
      location.toMap(),
      where: "id = ?",
      whereArgs: [location.id],
    );
    return id;
  }

  Future<Location> getLocation(int id) async {

    final db = await database;
    var res = await db.query("location", where: "id = ?", whereArgs: [id]);
    List<Location> list = res.isNotEmpty ? res.map((c) => Location.fromMap(c)).toList() : [];
    Location location;
    if (list.length==1){
      location = list[0];
    }
    return location;
  }

  deleteLocation(Location location) async {
    //todo: relations
    final db = await database;
    var res = await db.delete("location", where: "id = ?", whereArgs: [location.id]);
    return res;
  }



  // #################################################
  // personencounter

  Future<int> insertPersonEncounter(PersonEncounter personEncounter, {int encounter_id}) async {
    if (personEncounter.notes == ""){
      personEncounter.notes = null;
    }

    if (personEncounter.encounter_id == null && encounter_id != null){
      personEncounter.encounter_id = encounter_id;
    }
    Map<String, dynamic> map = personEncounter.toMap();
    final db = await database;
    int id = await db.insert(
      'person_encounter',
      map,
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return id;
  }


  Future<int> updatePersonEncounter(PersonEncounter personEncounter) async {
    if (personEncounter.notes == "") {
      personEncounter.notes = null;
    }

    Map<String, dynamic> map = personEncounter.toMap();
    final db = await database;
    int id = await db.update(
      'person_encounter',
      map,
      where: "id = ?",
      whereArgs: [personEncounter.id],
    );
    return id;
  }

  Future<List<PersonEncounter>> getPersonEncountersForEncounter(Encounter encounter) async {
    final db = await database;
    var res = await db.query(
      "person_encounter",
      where: "encounter_id = ?",
      whereArgs: [encounter.id],
    );

    List<PersonEncounter> list = [];
    if (res.isNotEmpty){

    for(var i=0;i<res.length;i++){
      PersonEncounter personEncounter = PersonEncounter.fromMap(res[i]);
      if (res[i]["person_id"] != null) {
        personEncounter.person = await getPerson(res[i]["person_id"]);
      }
      list.add(personEncounter);
    }

    }

    res.isNotEmpty ? res.map((c) => PersonEncounter.fromMap(c)).toList() : [];
    return list;
  }

  deletePersonEncounter(PersonEncounter personEncounter) async {
    final db = await database;
    var res = await db.delete("person_encounter", where: "id = ?", whereArgs: [personEncounter.id]);
    return res;
  }


  debugPersonEncounters() async{
    final db = await database;
    var res = await db.query("person_encounter");
    print("Encounters in db: ${res.length}");
    res.forEach((element) {
      print(element);
    });
  }

  // #################################################
  // encounter

  Future<List<Encounter>> getAllEncounters() async {

    final db = await database;
    var res = await db.query("encounter", orderBy: "start DESC");

    List<Encounter> list = [];
    if (res.isNotEmpty){
      for(var i=0;i<res.length;i++){
        Encounter encounter = Encounter.fromMap(res[i]);
        if (res[i]["location_id"] != null) {
          encounter.location = await getLocation(res[i]["location_id"]);
        }
        encounter.personEncounters = await getPersonEncountersForEncounter(encounter);
        list.add(encounter);
      }
    }
    return list;
  }


  Future<Encounter> getEncounter(int encounter_id) async {

    final db = await database;
    var res = await db.query(
      "encounter",
      where: "id = ?",
      whereArgs: [encounter_id],
    );

    List<Encounter> list = [];
    if (res.isNotEmpty){
      for(var i=0;i<res.length;i++){
        Encounter encounter = Encounter.fromMap(res[i]);
        if (res[i]["location_id"] != null) {
          encounter.location = await getLocation(res[i]["location_id"]);
        }
        list.add(encounter);
      }
    }

    Encounter encounter = null;
    if(list.length > 0) {
      encounter = list[0];
    }

    return encounter;
  }

  Future<int> insertEncounter(Encounter encounter) async {
    if (encounter.name == ""){
      encounter.name = null;
    }
    if (encounter.description == ""){
      encounter.description = null;
    }
    final db = await database;
    int id = await db.insert(
      'encounter',
      encounter.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    for(var i=0;i<encounter.personEncounters.length;i++) {
      await insertPersonEncounter(encounter.personEncounters[i], encounter_id: id);
    }
    return id;
  }

  Future<int> updateEncounter(Encounter encounter) async {
    if (encounter.name == ""){
      encounter.name = null;
    }
    if (encounter.description == ""){
      encounter.description = null;
    }

    final db = await database;
    int id = await db.update(
      'encounter',
      encounter.toMap(),
      where: "id = ?",
      whereArgs: [encounter.id],
    );

    List<PersonEncounter> previousPersonEncountersInDb = await getPersonEncountersForEncounter(encounter);

    for (var i=0; i<previousPersonEncountersInDb.length; i++) {
      if (encounter.personEncounters.contains(previousPersonEncountersInDb[i])){
        // everything ok
      } else {
        await deletePersonEncounter(previousPersonEncountersInDb[i]);
      }
    }

    for (var i=0; i<encounter.personEncounters.length; i++){
      if (previousPersonEncountersInDb.contains(encounter.personEncounters[i])){
        await updatePersonEncounter(encounter.personEncounters[i]);
      } else {
        await insertPersonEncounter(encounter.personEncounters[i], encounter_id: encounter.id);
      }
    }

    return id;
  }

  deleteEncounter(Encounter encounter) async {
    int encounter_id = encounter.id;
    final db = await database;

    await db.delete("person_encounter", where: "encounter_id = ?", whereArgs: [encounter_id]);
    await db.delete("encounter", where: "id = ?", whereArgs: [encounter.id]);
  }

  debugEncounters() async{
    final db = await database;
    var res = await db.query("encounter");
    print("Encounters in db: ${res.length}");
    res.forEach((element) {
      print(element);
    });
  }


  Future<List<List<dynamic>>>exportToCSV() async{
    final db = await database;
    var res = await db.query("person_encounter");
    List personEncounters = [];
    for(var i=0;i<res.length;i++){
      PersonEncounter personEncounter = PersonEncounter.fromMap(res[i]);
      if (res[i]["person_id"] != null) {
        personEncounter.person = await getPerson(res[i]["person_id"]);
      }
      personEncounters.add(personEncounter);
    }
    List<List<dynamic>> data = [];
    List<dynamic> titles = [
      "Name",
      "Mask",
      "Notes",
      "Encounter Name",
      "Encounter Desc",
      "Location Name",
      "Location Desc",
      "Start",
      "End",
        ];
    data.add(titles);

    for(var i=0;i<personEncounters.length;i++){
      if (personEncounters[i].encounter_id != null){
        Encounter encounter = await getEncounter(personEncounters[i].encounter_id);
        List encounterData = [];
        encounterData.add("${personEncounters[i].person.first_name} ${personEncounters[i].person.last_name}");
        encounterData.add(personEncounters[i].mask ? "mask" : "no mask");
        encounterData.add("${personEncounters[i].notes}");
        encounterData.add("${encounter?.name}");
        encounterData.add("${encounter?.description}");
        encounterData.add("${encounter?.location?.name}");
        encounterData.add("${encounter?.location?.description}");
        encounterData.add("${encounter?.start?.toIso8601String()}");
        encounterData.add("${encounter?.end?.toIso8601String()}");
        data.add(encounterData);
      }


    }
  return data;
  }




}