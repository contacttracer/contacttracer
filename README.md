# contacttracer

Track your encounters with other persons conveniently on your smartphone.

The data is stored on the device, nothing is transmitted into the internet.

It's a flutter app, so it will (hopefully) be available on android and iOS at some point.


## Installation
Until it is availiable in the app stores, contacttracer can only be installed on android. 
Install flutter, connect your device and run  `flutter run`. Then choose your device.

Alternatively, compile an APK using `flutter build apk`. Then install the apk to your device.

## Models

### Person
Whom did I meet?
* First Name
* Last Name
* Phone
* Email

### Location
Where have I been?
* Name
* Description

### PersonEncounter
When I met person X, did they ware a mask? Was there anything noteworthy?
* Bool mask
* Description
* person_id
* encounter_id

### Encounter
I went to this place at this time. I met persons x, y, and z. x wore a mask, the others not.
* Name
* Description
* Location
* Start
* End
* List of PersonEncounters